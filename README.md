# ds167-configs

DS167 is an Allwinner A20 based Home Automation board.

Here is uboot and kernel config files for ds167.

Tested uboot: v2020.04 tag
Tested Kernel: linux-sunxi 5.7.0-rc4
Tested Rootfs: Ubuntu Armhf 20.04 focal fossa

- SDcard OK
- EMMC OK
- 100M Ethernet OK
- USB2.0 OK
- STATUS-LED OK
- GPIO OK
- UART3 OK

