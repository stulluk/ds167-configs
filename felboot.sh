#!/bin/bash

BASEDIR="/D/DS167/"

cd ${BASEDIR}

if nc -z -w1 192.168.1.25 22
then
	printf "SSH is online, rebooting\n"
	ssh i10 reboot
	sleep 5
else
	printf "SSH is offline\n"
fi

sudo sunxi-fel -v -p \
	uboot boot/u-boot/u-boot-sunxi-with-spl.bin \
	write 0x42000000 kernel/linux-sunxi/arch/arm/boot/zImage \
	write 0x43000000 kernel/linux-sunxi/arch/arm/boot/dts/sun7i-a20-drejo-ds167-emmc.dtb \
        write 0x43100000 boot/boot.cmd/boot.scr.nfs
