#!/bin/bash

BASEDIR="/D/DS167"

if nc -z -w1 192.168.1.25 22
then
	printf "online, rebooting\n"
	ssh i10 reboot
	sleep 5
else
	printf "offline\n"
fi


sudo sunxi-fel -v -p \
	uboot ${BASEDIR}/boot/u-boot/u-boot-sunxi-with-spl.bin \
        write 0x43100000 ${BASEDIR}/boot/boot.cmd/boot.scr.logo \
	write 0x43200000 boot/boot.bmp 
	#write 0x44200000 boot/boot2.bmp \
	#write 0x45200000 boot/boot3.bmp 
	
