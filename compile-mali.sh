#!/bin/bash

cd sunxi-mali
export CROSS_COMPILE="arm-linux-gnueabihf-"
export KDIR="/D/DS167/kernel/linux-sunxi/"
# export INSTALL_MOD_PATH="/D/DS167/rootfs/focal-base-armhf/lib/modules/"
export INSTALL_MOD_PATH="/D/DS167/rootfs/mali/"
./build.sh -r r6p2 -b
./build.sh -r r6p2 -i
